package com.lutao.service.impl;

import com.lutao.service.TestService1;

public class TestService1Impl implements TestService1 {

    @Override
    public String sayHello(String name) {
        return "Test1: Hello " + name + "!";
    }

}
