package com.lutao.service.impl;

import com.lutao.service.TestService2;

public class TestService2Impl implements TestService2 {

    @Override
    public String sayHello(String name) {
        return "Test2: Hello " + name + "!";
    }

}
