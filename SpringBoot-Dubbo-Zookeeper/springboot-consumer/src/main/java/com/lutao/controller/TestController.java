package com.lutao.controller;

import com.alibaba.fastjson.JSONObject;
import com.lutao.service.TestService1;
import com.lutao.service.TestService2;
import com.lutao.service.TestService3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    TestService1 testService1;
    @Autowired
    TestService2 testService2;
    @Autowired
    TestService3 testService3;
    /**
     * 测试 JSON 接口；
     *
     * @param name 名字；
     * @return
     */
    @ResponseBody
    @RequestMapping("/test1/{name}")
    public JSONObject test1Json(@PathVariable("name") String name) {
        JSONObject jsonObject = new JSONObject();
        String testStr = testService1.sayHello(name);
        jsonObject.put("str", testStr);
        return jsonObject;
    }
    @ResponseBody
    @RequestMapping("/test2/{name}")
    public JSONObject test2Json(@PathVariable("name") String name) {
        JSONObject jsonObject = new JSONObject();
        String testStr = testService2.sayHello(name);
        jsonObject.put("str", testStr);
        return jsonObject;
    }
    @ResponseBody
    @RequestMapping("/test3/{name}")
    public JSONObject test3Json(@PathVariable("name") String name) {
        JSONObject jsonObject = new JSONObject();
        String testStr = testService3.sayHello(name);
        jsonObject.put("str", testStr);
        return jsonObject;
    }
}
