package com.lutao.service.impl;

import com.lutao.service.TestService3;

public class TestService3Impl implements TestService3 {

    @Override
    public String sayHello(String name) {
        return "Test3: Hello " + name + "!";
    }

}
