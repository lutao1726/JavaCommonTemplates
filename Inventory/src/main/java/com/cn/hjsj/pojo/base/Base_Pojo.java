package com.cn.hjsj.pojo.base;

import java.io.Serializable;

import com.cn.hjsj.util.time.TimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public abstract class Base_Pojo implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private  Object id;

    private String createDate;

    private String createBy;

    private String updateBy;

    private String updateDate;

    private String memo1;

    private String memo2;

    private String memo3;

    private String memo4;

    private String memo5;

    private String recordStatus;

    private String description;


    public String getMemo1() {
        return memo1;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public void setMemo1(String memo1) {
        this.memo1 = memo1;
    }

    public String getMemo2() {
        return memo2;
    }

    public void setMemo2(String memo2) {
        this.memo2 = memo2;
    }

    public String getMemo3() {
        return memo3;
    }

    public void setMemo3(String memo3) {
        this.memo3 = memo3;
    }

    public String getMemo4() {
        return memo4;
    }

    public void setMemo4(String memo4) {
        this.memo4 = memo4;
    }

    public String getMemo5() {
        return memo5;
    }

    public void setMemo5(String memo5) {
        this.memo5 = memo5;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void createInitialization(String user)
    {
        this.createBy=user;
        this.updateBy=user;
    }

    public void UpdateInitialization(String user)
    {
        this.updateBy=user;
        this.updateDate=TimeUtil.getNowStr();
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate){
        this.createDate = TimeUtil.changeDatestr(createDate);
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = TimeUtil.changeDatestr(updateDate);
    }


}
