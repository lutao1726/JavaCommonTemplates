package com.cn.hjsj.service.impl;

import com.cn.hjsj.dao.ITestDao;
import com.cn.hjsj.service.ITestService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/6/15.
 */
@Component("testService")
public class TestServiceImpl implements ITestService {

    @Resource(name="ITestDao")
    private ITestDao testDao;

    @Override
    public Integer getListCount()
    {
        return this.testDao.getListCount();
    }

}
