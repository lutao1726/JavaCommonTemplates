package com.cn.hjsj.controller;

import com.cn.hjsj.base.annotation.Permission;
import com.cn.hjsj.service.ITestService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/6/15.
 */
@Controller
@RequestMapping("/testController")
public class TestController {
    @Resource(name = "testService")
    private ITestService testService;

    //查询账户余额
    @RequestMapping(value = "/test")
    @ResponseBody
    @Permission("1231")
    public Object test(@RequestBody String jsonstring) {
        System.out.println("---------------2");
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> map1 = new HashMap<String, Object>();
        int i = testService.getListCount();
        map1.put("count", i);
        map.put("code", "10000");
        map.put("mes", "成功");
        map.put("data", map1);
        System.out.println("---------------1");
        return map;
    }
}
