package com.cn.hjsj.dao;

import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/6/15.
 */
@Repository("ITestDao")
public interface ITestDao {

    public Integer getListCount();

}
