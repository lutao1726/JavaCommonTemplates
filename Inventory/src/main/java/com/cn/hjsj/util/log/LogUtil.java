package com.cn.hjsj.util.log;

import org.apache.log4j.Logger;

public class LogUtil {
	private static Logger log=Logger.getLogger("OutAgency");
	public static void Debug(String Message)
	{
		log.debug(Message);
	}
	public static void info(String Message)
	{
		log.info(Message);
	}
	public static void warn(String Message)
	{
		log.warn(Message);
	}
	public static void error(String Message)
	{
		log.error(Message);
	}
	public static void fatal(String Message)
	{
		log.fatal(Message);
	}
}
