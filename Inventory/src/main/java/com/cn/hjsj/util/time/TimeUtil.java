package com.cn.hjsj.util.time;

import com.cn.hjsj.util.log.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static String getNowStr()
	{
		try {
			return df.format(new Date());
		} catch (Exception e) {
			return "1900-01-01 00:00:00";
		}
		
	}
	
	public static String changeDatestr(String date){
		String newdate = date;
		try{
			Date date1 = df.parse(date);
		    newdate=df.format(date1);
		}catch(Exception e){
			LogUtil.error("日期转化错误:" + e.getMessage());
		}
		 
		 return newdate;
	}
	
	

}
