package com.cn.hjsj.util.sequence;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SeqFactory {
	private static int i=100000;
	private static int j=0;
   public synchronized static String  GetSeq(String appkey) {
	  Date now=new Date();
	  SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
	  StringBuilder sb=new StringBuilder("oplat");
	  sb.append(sdf.format(now));
	  sb.append(appkey.substring(0, 3));
	 sb.append(i);
	 i++;
	 if(i>999000){i=100000;}
	 return sb.toString();
}
   public synchronized static String  GetEopSeq(String appkey) {
		  Date now=new Date();
		  SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSSS");
		  StringBuilder sb=new StringBuilder("o2000070001");
		  sb.append(sdf.format(now));
		 
		 return sb.toString();
	}
   
   //充值订单号
   public synchronized static String  GetCzSeq(String name) {
		  Date now=new Date();
		  SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
		  StringBuilder sb=new StringBuilder(name);
		  sb.append(sdf.format(now));
		  sb.append(i);
		 i++;
		 if(i>999000){i=100000;}
		 return sb.toString();
	}
   
   private static Map<String,String> SeqMapping;
	static{
		SeqMapping=new HashMap<String,String>();
	}
	public static void setSeq(String ours,String others) {
		SeqMapping.put(ours, others);
	}
	public static String getSeq(String ours) {
		
		return SeqMapping.get(ours);
	}
	public static String getSeqRemove(String ours)
	{
		return SeqMapping.remove(ours);
	}
}
