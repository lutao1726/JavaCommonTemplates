package com.cn.hjsj.util.spring;

import org.springframework.beans.BeansException;  
import org.springframework.context.ApplicationContext;  
import org.springframework.context.ApplicationContextAware;  
import org.springframework.stereotype.Component;
  
@Component("SpringUtil")
public class SpringUtil implements ApplicationContextAware {  
      
    private static ApplicationContext applicationContext;  
      
    @Override  
    public void setApplicationContext(ApplicationContext context)  
        throws BeansException {  
        SpringUtil.applicationContext = context;  
    }  
    public static Object getBean(String name){  
        return applicationContext.getBean(name);  
    }  
}  