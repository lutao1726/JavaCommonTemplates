package com.cn.hjsj.util.normal;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Properties;
import java.io.InputStream;
import java.io.IOException;

public final class FileUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 获取真实文件名（自动去掉文件路径）
     */
    public static String getRealFileName(String fileName) {
        return FilenameUtils.getName(fileName);
    }

    /**
     * 创建文件
     */
    public static File createFile(String filePath) {
        File file;
        try {
            file = new File(filePath);
            File parentDir = file.getParentFile();
            if (!parentDir.exists()) {
                FileUtils.forceMkdir(parentDir);
            }
        } catch (Exception e) {
            LOGGER.error("create file failure", e);
            throw new RuntimeException(e);
        }
        return file;
    }
    public static int getPropertyValueInt(String path,String par)
    {
        Properties prop = new Properties();
        InputStream in = Object.class.getResourceAsStream(path);
        try {
            prop.load(in);
            return  Integer.parseInt(prop.getProperty(par).trim());
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String getPropertyValue(String path,String par)
    {
        Properties prop = new Properties();
        InputStream in = Object.class.getResourceAsStream(path);
        try {
            prop.load(in);
            return  prop.getProperty(par).trim();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean getPropertyValueBoolean(String path,String par)
    {
        Properties prop = new Properties();
        InputStream in = Object.class.getResourceAsStream(path);
        try {
            prop.load(in);
            return  Boolean.parseBoolean(prop.getProperty(par).trim());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
