package com.cn.hjsj.util.DecimalUtil;
import com.cn.hjsj.util.log.LogUtil;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.math.RoundingMode;

public class DecimalUtil {

    public static String calculate(String str1,String str2,String operator){
        BigDecimal bigDecimal1 = new BigDecimal(str1);
        BigDecimal bigDecimal2 = new BigDecimal(str2);
        String result =null;

        // operator     1-加,2-减,3-乘,4-除   对应加减乘除 四个运算

        if ("1".equals(operator)){
            result=bigDecimal1.add(bigDecimal2).toString();
        }else if ("2".equals(operator)) {
            result = bigDecimal1.subtract(bigDecimal2).toString();
        }else if ("3".equals(operator)){
            DecimalFormat df=new DecimalFormat("###,##0.00");
            df.setRoundingMode(RoundingMode.UP);
            result=df.format(bigDecimal1.multiply(bigDecimal2)).toString();
//            result=bigDecimal1.multiply(bigDecimal2).toString();
        }else if ("4".equals(operator)){
            //默认保留两位
            result=bigDecimal1.divide(bigDecimal2,2).toString();
        }else{
            result="error";
            LogUtil.info("运算符输入错误");
        }
        return  result;
    }
}
