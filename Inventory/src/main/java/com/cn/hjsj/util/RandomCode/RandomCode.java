package com.cn.hjsj.util.RandomCode;

import java.util.Random;

public class RandomCode {

    public static String getRandom() {
        String a="1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 8; i++) {
            int number = random.nextInt(a.length());
            sb.append(a.charAt(number));
        }
        return sb.toString();

    }
}
