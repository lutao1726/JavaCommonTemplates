package com.cn.hjsj.util.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.cn.hjsj.util.log.*;

public class HttpUtil {
	 public static StringBuilder sendPost(String url, String param) {
		 if (url==null)
		 {
			 return new StringBuilder("NETWORKURLERROR");
		 }
	        PrintWriter out = null;
	        BufferedReader in = null;
	        StringBuilder result = new StringBuilder("");
	        try {
	            URL realUrl = new URL(url);
	            // 打开和URL之间的连接
	            URLConnection conn = realUrl.openConnection();
	            // 设置通用的请求属性
	            conn.setRequestProperty("accept", "*/*");
	            conn.setRequestProperty("connection", "Keep-Alive");
	            
//	            conn.setRequestProperty("user-agent",
//	                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	            // 发送POST请求必须设置如下两行
	            conn.setDoOutput(true);
	            conn.setDoInput(true);
	            // 获取URLConnection对象对应的输出流
	            out = new PrintWriter(conn.getOutputStream());
	            // 发送请求参数
	            out.print(param);
	            // flush输出流的缓冲
	            out.flush();
	            
	            // 定义BufferedReader输入流来读取URL的响应
	            in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream(),"UTF-8"));
	            String line;
	            while ((line = in.readLine()) != null) {
	            	result.append(line);
	            }
	        } catch (Exception e) {
	        	LogUtil.error("发送 POST 请求出现异常！"+e);
	            e.printStackTrace();
	            return new StringBuilder("NETWORKERROR");
	        }
	        //使用finally块来关闭输出流、输入流
	        finally{
	            try{
	                if(out!=null){
	                    out.close();
	                }
	                if(in!=null){
	                    in.close();
	                }
	            }
	            catch(IOException ex){
	            	LogUtil.error("io流关闭错误！"+ex);
	                ex.printStackTrace();
	            }
	        }
	        return result;
	    }  
	 
	//发送json请求
	 public static StringBuilder sendJsonPost(String url, String param) {
		 if (url==null)
		 {
			 return new StringBuilder("NETWORKURLERROR");
		 }
	        PrintWriter out = null;
	        BufferedReader in = null;
	        StringBuilder result = new StringBuilder("");
	        	try {  
	        		LogUtil.info(url + ">>>>>>" + param);
	        		URL realUrl = new URL(url);// 创建连接  
	                HttpURLConnection connection = (HttpURLConnection)realUrl.openConnection();  
	                connection.setDoOutput(true);  
	                connection.setDoInput(true);  
	                connection.setUseCaches(false);  
	                connection.setInstanceFollowRedirects(true);  
	                connection.setRequestMethod("POST"); // 设置请求方式  
	                connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式  
	                connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式  
	                connection.connect();  
	                OutputStreamWriter out1 = new OutputStreamWriter(  
	                        connection.getOutputStream(), "UTF-8"); // utf-8编码  
	                out1.append(param);  
	                out1.flush();  
	                out1.close();  
	                // 读取响应  
	             // 定义BufferedReader输入流来读取URL的响应
		            in = new BufferedReader(
		                    new InputStreamReader(connection.getInputStream(),"UTF-8"));
		            String line;
		            while ((line = in.readLine()) != null) {
		            	result.append(line);
		            }
	            
	        } catch (Exception e) {
	        	LogUtil.error("发送 POST 请求出现异常！"+e);
	            e.printStackTrace();
	            return new StringBuilder("NETWORKERROR");
	        }
	        //使用finally块来关闭输出流、输入流
	        finally{
	            try{
	                if(out!=null){
	                    out.close();
	                }
	                if(in!=null){
	                    in.close();
	                }
	            }
	            catch(IOException ex){
	            	LogUtil.error("io流关闭错误！"+ex);
	                ex.printStackTrace();
	            }
	        }
	        return result;
	    }  
}
