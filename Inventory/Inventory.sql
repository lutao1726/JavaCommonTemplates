/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.24 : Database - ty_inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`inventory` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `inventory`;

/*Table structure for table `inventory` */

DROP TABLE IF EXISTS `inventory`;

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `company` varchar(255) DEFAULT NULL COMMENT '行政公司',
  `area` varchar(255) DEFAULT NULL COMMENT '区域',
  `warehouse` varchar(255) DEFAULT NULL COMMENT '门店-仓库',
  `warehouseName` varchar(255) DEFAULT NULL COMMENT '门店-仓库名称',
  `storeAttributes` varchar(255) DEFAULT NULL COMMENT '门店属性',
  `materialBig` varchar(255) DEFAULT NULL COMMENT '物料大类',
  `materialMid` varchar(255) DEFAULT NULL COMMENT '物料中类(手机制式)',
  `materialSmall` varchar(255) DEFAULT NULL COMMENT '物料小类',
  `materialModel` varchar(255) DEFAULT NULL COMMENT '物料型号',
  `materialCode` varchar(255) DEFAULT NULL COMMENT '物料编码',
  `materialTips` varchar(255) DEFAULT NULL COMMENT '物料说明',
  `serviceAttribute` varchar(255) DEFAULT NULL COMMENT '业务属性',
  `planner` varchar(255) DEFAULT NULL COMMENT '计划员',
  `sales` varchar(255) DEFAULT NULL COMMENT '销量',
  `endingCount` varchar(255) DEFAULT NULL COMMENT '期末数量',
  `transferin` varchar(255) DEFAULT NULL COMMENT '调拨在途',
  `inventory` varchar(255) DEFAULT NULL COMMENT '库存',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `createBy` varchar(50) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1410 DEFAULT CHARSET=utf8;

/*Data for the table `inventory` */

LOCK TABLES `inventory` WRITE;

insert  into `inventory`(`id`,`company`,`area`,`warehouse`,`warehouseName`,`storeAttributes`,`materialBig`,`materialMid`,`materialSmall`,`materialModel`,`materialCode`,`materialTips`,`serviceAttribute`,`planner`,`sales`,`endingCount`,`transferin`,`inventory`,`createTime`,`createBy`) values (1368,'lutao','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','2017-08-28 08:52:28','lutao'),(1369,'2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','2017-08-28 08:52:28','lutao'),(1370,'3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','2017-08-28 08:52:28','lutao'),(1371,'4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','2017-08-28 08:52:28','lutao'),(1372,'5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','2017-08-28 08:52:28','lutao'),(1373,'6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','2017-08-28 08:52:28','lutao'),(1374,'7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','2017-08-28 08:52:28','lutao'),(1375,'8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','2017-08-28 08:52:28','lutao'),(1376,'9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','2017-08-28 08:52:28','lutao'),(1377,'10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','2017-08-28 08:52:28','lutao'),(1378,'11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','2017-08-28 08:52:28','lutao'),(1379,'12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','2017-08-28 08:52:28','lutao'),(1380,'13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','2017-08-28 08:52:28','lutao'),(1381,'14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','2017-08-28 08:52:28','lutao'),(1382,'15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','2017-08-28 08:52:28','lutao'),(1383,'16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','2017-08-28 08:52:28','lutao'),(1384,'17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','2017-08-28 08:52:28','lutao'),(1385,'18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','2017-08-28 08:52:28','lutao'),(1386,'19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','2017-08-28 08:52:28','lutao'),(1387,'20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','2017-08-28 08:52:28','lutao'),(1388,'lutao','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','2017-08-28 08:52:28','lutao'),(1389,'lutao','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','2017-08-28 09:13:45','lutao'),(1390,'2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','2017-08-28 09:13:45','lutao'),(1391,'3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','2017-08-28 09:13:45','lutao'),(1392,'4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','2017-08-28 09:13:45','lutao'),(1393,'5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','2017-08-28 09:13:45','lutao'),(1394,'6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','2017-08-28 09:13:45','lutao'),(1395,'7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','2017-08-28 09:13:45','lutao'),(1396,'8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','2017-08-28 09:13:45','lutao'),(1397,'9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','2017-08-28 09:13:45','lutao'),(1398,'10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','2017-08-28 09:13:45','lutao'),(1399,'11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','2017-08-28 09:13:45','lutao'),(1400,'12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','2017-08-28 09:13:45','lutao'),(1401,'13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','2017-08-28 09:13:45','lutao'),(1402,'14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','2017-08-28 09:13:45','lutao'),(1403,'15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','2017-08-28 09:13:45','lutao'),(1404,'16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','2017-08-28 09:13:45','lutao'),(1405,'17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','2017-08-28 09:13:45','lutao'),(1406,'18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','2017-08-28 09:13:45','lutao'),(1407,'19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','2017-08-28 09:13:45','lutao'),(1408,'20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','2017-08-28 09:13:45','lutao'),(1409,'lutao','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','2017-08-28 09:13:45','lutao');

UNLOCK TABLES;

/*Table structure for table `system_user` */

DROP TABLE IF EXISTS `system_user`;

CREATE TABLE `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `userName` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `status` int(5) NOT NULL DEFAULT '0' COMMENT '状态0/1(可用/不可用)',
  `power` int(5) NOT NULL DEFAULT '0' COMMENT '权限3/1/2（导入导出/导入/导出）',
  `memo1` varchar(255) DEFAULT NULL COMMENT '预留1',
  `memo2` varchar(255) DEFAULT NULL COMMENT '预留2',
  `memo3` varchar(255) DEFAULT NULL COMMENT '预留3',
  `memo4` varchar(255) DEFAULT NULL COMMENT '预留4',
  `memo5` varchar(255) DEFAULT NULL COMMENT '预留5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `system_user` */

LOCK TABLES `system_user` WRITE;

insert  into `system_user`(`id`,`userName`,`password`,`status`,`power`,`memo1`,`memo2`,`memo3`,`memo4`,`memo5`) values (1,'admin','admin',0,1,NULL,NULL,NULL,NULL,NULL),(2,'lutao','123',0,3,NULL,NULL,NULL,NULL,NULL),(3,'lcc','123',0,2,NULL,NULL,NULL,NULL,NULL);

UNLOCK TABLES;

/*!50106 set global event_scheduler = 1*/;

/* Event structure for event `inventory` */

/*!50106 DROP EVENT IF EXISTS `inventory`*/;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
