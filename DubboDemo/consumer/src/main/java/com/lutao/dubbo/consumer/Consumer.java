package com.lutao.dubbo.consumer;

import com.lutao.dubbo.api.DemoLtService;
import com.lutao.dubbo.api.User;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by Z on 2017-03-29.
 */
public class Consumer {
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                new String[]{"applicationContext.xml"});
        context.start();
        final DemoLtService demoService = (DemoLtService) context.getBean("demoLtService");

        //测试
        String hello = demoService.sayHello("Tom");
        System.out.println(hello);
        List<User> users = demoService.getUsers();
        for(User user:users){
            System.out.println(user.toString());
        }
        System.in.read(); // 为保证服务一直开
    }
}
