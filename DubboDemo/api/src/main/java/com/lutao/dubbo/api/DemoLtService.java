package com.lutao.dubbo.api;

import java.util.List;

/**
 * Created by Z on 2017-03-29.
 */
public interface DemoLtService {
    String sayHello(String name);

    public List<User> getUsers();
}
