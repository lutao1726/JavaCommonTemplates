package com.lutao.dubbo.provider;

import com.lutao.dubbo.api.DemoLtService;
import com.lutao.dubbo.api.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LT on 2017-03-29.
 */
public class DemoLtServiceImpl implements DemoLtService {
    public String sayHello(String name) {
        System.out.println("name:"+name);
        return "Hello " + name;
    }

    public List<User> getUsers() {
        List<User> list = new ArrayList<User>();
        User u1 = new User();
        u1.setName("jack");
        u1.setAge(20);
        u1.setSex("m");

        list.add(u1);
        System.out.println(list.get(0).toString());
        return list;
    }
}
