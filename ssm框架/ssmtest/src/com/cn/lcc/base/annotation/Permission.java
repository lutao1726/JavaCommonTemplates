﻿package com.cn.lcc.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   自定义标签   可用此标签校验规定参数
 */
@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.METHOD)  
public @interface Permission {  
  
    String value();  
      
}  