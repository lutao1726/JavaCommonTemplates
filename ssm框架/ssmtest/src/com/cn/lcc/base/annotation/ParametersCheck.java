package com.cn.lcc.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   参数检查
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ParametersCheck {

    FieldCheck[] checks() default {};

}
