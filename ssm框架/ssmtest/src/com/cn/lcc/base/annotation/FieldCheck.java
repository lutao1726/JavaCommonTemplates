package com.cn.lcc.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   自定义标签  用于参数获取解析 验证
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface FieldCheck {

    String name() default ""; // 参数名称

    String desc() default ""; // 字段描述

    int length() default 0; // 字段长度，汉字算2

    boolean allowNull() default false; // 是否允许该字段为空

}
