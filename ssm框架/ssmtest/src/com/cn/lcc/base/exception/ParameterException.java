package com.cn.lcc.base.exception;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   参数异常
 */
@SuppressWarnings("serial")
public class ParameterException extends BaseException {

    public ParameterException(HttpServletRequest req,String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return ExceptionCode.ParameterIsEmpty.getCode();
    }
}
