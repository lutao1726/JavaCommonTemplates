package com.cn.lcc.base.exception;

import javax.servlet.http.HttpServletRequest;



/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   json异常
 */
@SuppressWarnings("serial")
public class JsonException extends BaseException {

    public JsonException(HttpServletRequest req,String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return ExceptionCode.jsonError.getCode();
    }
}
