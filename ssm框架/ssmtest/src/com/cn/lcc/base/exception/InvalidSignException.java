package com.cn.lcc.base.exception;


/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   签名异常
 */
@SuppressWarnings("serial")
public class InvalidSignException extends BaseException {

    public InvalidSignException() {
        super("签名错误");
    }

    @Override
    public String getCode() {
        return ExceptionCode.InvalidSign.getCode();
    }
}
