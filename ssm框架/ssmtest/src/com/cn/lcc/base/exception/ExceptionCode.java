package com.cn.lcc.base.exception;
/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   自定义异常编码及备注
 */
public enum ExceptionCode {

    ParameterIsEmpty("900001", "参数为空"),
    InvalidSign("900002", "签名错误"),
    jsonError("900003", "json格式错误");
    private String code = null;
    private String desc = null;

    private ExceptionCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
