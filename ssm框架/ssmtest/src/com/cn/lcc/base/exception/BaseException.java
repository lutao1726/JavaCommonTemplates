package com.cn.lcc.base.exception;


/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   异常基类
 */
@SuppressWarnings("serial")
public abstract class BaseException extends Exception {

    public BaseException(String string) {
        super(string);

    }

    public abstract String getCode();

}
