package com.cn.lcc.base.common;


/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   返回json格式
 */
public class ServerResponse {

    private boolean success = false;
    private String message = null;
    private Object data = null;
    private String code = null;

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
