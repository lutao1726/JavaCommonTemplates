﻿package com.cn.lcc.base.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import com.cn.lcc.base.annotation.Permission;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   拦截器   在spring-mvc.xml 中配置
 * 		 自定义标签检验    utf-8编码设置
 */
public class FreeMarkerViewInterceptor extends HandlerInterceptorAdapter
{
  public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
    throws Exception
  {
  }

  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView view)
    throws Exception
  {
    String contextPath = request.getContextPath();
    if (view != null)
      request.setAttribute("base", contextPath);
      response.setCharacterEncoding("UTF-8");

  }

  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
  {
	  request.setCharacterEncoding("utf-8");

    if ((handler instanceof ResourceHttpRequestHandler)) {
      return true;
    }

    HandlerMethod method = (HandlerMethod)handler;
    Permission permission = (Permission)method.getMethodAnnotation(Permission.class);
    String perString = null;
    if (permission == null)
      perString = "null";
    else {
      perString = permission.value();
    }

    if ("pass".equals(perString))
    {
      return true;
    }
    //这边自己实现
    return true;
  }
}