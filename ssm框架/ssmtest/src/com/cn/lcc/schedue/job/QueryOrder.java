package com.cn.lcc.schedue.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cn.lcc.dao.OrderDao;
import com.cn.lcc.pojo.Order;
import com.cn.lcc.schedue.task.TaskType;
import com.cn.lcc.schedue.task.TaskUtil;
import com.cn.lcc.schedue.work.QueryPayInfoTask;
import com.cn.lcc.util.spring.SpringUtil;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   查询订单 定时任务
 */
public class QueryOrder {

	private static Logger logger = LoggerFactory
			.getLogger(QueryOrder.class);

	/*
	 * 方法名与配置文件对应
	 */
	
	/**
	 *  这边示例一个查询订单然后对订单进行操作的过程
	 */
	public void getOrders() {
		if (TaskUtil.isPoolFull(TaskType.testType)) {
			logger.info("任务队列已满，任务即将退出，任务类型：{}",
					TaskType.testType.getTypeName());
			return;
		}
		logger.info("{} 开始...",TaskType.testType.getTypeName());
		OrderDao orderDao=(OrderDao)SpringUtil.getBean("orderDao");
		 Map<String, Object> map=new HashMap<String, Object>();
		 List<String> list = new ArrayList<String>();
		 list.add("test1");
		 map.put("orderStatus", "充值中");
		 map.put("query", "query");
		 map.put("payStatus", "支付成功");
		 map.put("list", list);
		List<Order> datas=orderDao.getOrderList(map);
		for (Order data : datas) {
			QueryPayInfoTask task = new QueryPayInfoTask();
			task.setMemo1(data.getAccountNo());
			task.setMemo2(data.getOrderNo());
			// 进入work
			TaskUtil.execute(TaskType.testType, task);
		}
		logger.info("{} 完毕",TaskType.testType.getTypeName());
	}
}
