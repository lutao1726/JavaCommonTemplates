package com.cn.lcc.schedue.task;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   task基类
 */
public abstract class Task implements Runnable {

    @Override
    public abstract void run();

    public abstract void recover();

}
