package com.cn.lcc.schedue.task;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   任务池
 */
public class TaskPool {

    private ThreadPoolExecutor pool = null;
    private BlockingQueue<Runnable> taskQueue = null;

    public ThreadPoolExecutor getPool() {
        return this.pool;
    }

    public void setPool(ThreadPoolExecutor pool) {
        this.pool = pool;
    }

    public BlockingQueue<Runnable> getTaskQueue() {
        return this.taskQueue;
    }

    public void setTaskQueue(BlockingQueue<Runnable> taskQueue) {
        this.taskQueue = taskQueue;
    }

}
