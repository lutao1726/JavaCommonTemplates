package com.cn.lcc.schedue.task;


/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   任务类型
 */
public enum TaskType {


    testType(1, "测试类型");

    private int typeId = 0;
    private String typeName = null;

    private TaskType(int typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public int getTypeId() {
        return this.typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
