﻿
package com.cn.lcc.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cn.lcc.base.annotation.FieldCheck;
import com.cn.lcc.base.annotation.ParametersCheck;
import com.cn.lcc.base.annotation.Permission;
import com.cn.lcc.base.common.ServerResponse;
import com.cn.lcc.dao.OrderDao;
import com.cn.lcc.pojo.Order;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   用例
 */
@Controller
@RequestMapping("/testControl")
public class TestController {
	private static Logger logger = LoggerFactory.getLogger(TestController.class);
	@Resource
	private OrderDao orderDao;
	
	@RequestMapping(value = "/test", method = RequestMethod.POST)
	@ParametersCheck(checks = {
			@FieldCheck(name = "orderNo", desc = "商户订单号", length = 50),
			@FieldCheck(name = "accountNo", desc = "手机号", length = 13)})
	@ResponseBody
	@Permission("pass")
    public ServerResponse test(HttpServletRequest req, HttpServletResponse res)
            throws Exception {
        ServerResponse response = new ServerResponse();
        Order order = new Order();
        order.setOrderNo(req.getParameter("orderNo"));
        order.setAccountNo(req.getParameter("accountNo"));
        order.setTableName("test1");
        orderDao.insertOrder(order);
        response.setSuccess(true);
        response.setCode("10000000");
        response.setMessage("交易处理中");
        return response;
    }
	
	
	
}
