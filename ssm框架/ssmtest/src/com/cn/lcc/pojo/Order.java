package com.cn.lcc.pojo;

import com.cn.lcc.pojo.base.Base_Pojo;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   订单bean
 */
@SuppressWarnings("serial")
public class Order extends Base_Pojo{
	private String orderNo;  // 系统自身订单号	
	private String accountNo;  // 充值账号
	private String orderStatus;  //  订单状态    
	private String payStatus;     // 支付状态
	private String amount;      // 充值金额
	private String payamount;    //支付金额
	private String tableName;    // 表名  用于动态插表
	
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayamount() {
		return payamount;
	}
	public void setPayamount(String payamount) {
		this.payamount = payamount;
	}
	
	
}
