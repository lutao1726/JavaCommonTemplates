package com.cn.lcc.pojo.base;

import java.io.Serializable;

import com.cn.lcc.util.tools.TimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   基类
 * 
 * 实体转json，字段为空不参与
 * 
 * 两个方法
 * createInitialization(String user)
   UpdateInitialization(String user)
 */
@JsonInclude(Include.NON_NULL)
public abstract class Base_Pojo implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String createBy;
	
	private String createDate;
	
	private String updateBy;
	
	private String updateDate;

	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void createInitialization(String user) {
		this.createBy = user;
		this.updateBy = user;
	}

	public void UpdateInitialization(String user) {
		this.updateBy = user;
		this.updateDate = TimeUtil.getNowStr();
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = TimeUtil.changeDatestr(createDate);
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = TimeUtil.changeDatestr(updateDate);
	}

}
