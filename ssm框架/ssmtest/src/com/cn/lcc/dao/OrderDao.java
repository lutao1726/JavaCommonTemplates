package com.cn.lcc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.cn.lcc.pojo.Order;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   control 直连dao层   
 * 
 * component  该标签不写也不影响control正常访问    此标签在定时器任务中初始化使用
 */
@Component("orderDao")
public interface OrderDao {
	void insertOrder(Order order);
	Order getOrder(Map map);
	void updateOrder(Order order);
	List<Order> getOrderList(Map map);
}