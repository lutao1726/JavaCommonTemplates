package com.cn.lcc.util.config;

import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * @author lcc
 * @date 2017年6月12日
 * @备注 配置文件参数获取类 三个方法 getInt getString getBoolean
 */
public class Config {

	private static PropertiesConfiguration config = PropertiesLoader
			.getConfiguration("config");

	// 后一个参数为取值为空时的默认值

	public static int getInt(String key) {
		return config.getInt(key, 0);
	}

	public static String getString(String key) {
		return config.getString(key, "");
	}

	public static boolean getBoolean(String key) {
		return config.getBoolean(key, false);
	}
}
