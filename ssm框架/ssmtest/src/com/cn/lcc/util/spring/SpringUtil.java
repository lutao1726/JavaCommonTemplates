package com.cn.lcc.util.spring;

import org.springframework.beans.BeansException;  
import org.springframework.context.ApplicationContext;  
import org.springframework.context.ApplicationContextAware;  
import org.springframework.stereotype.Component;
  
/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   spring工具类
 */
@Component("SpringUtil")
public class SpringUtil implements ApplicationContextAware {  
      
    private static ApplicationContext applicationContext;  
      
    @Override  
    public void setApplicationContext(ApplicationContext context)  
        throws BeansException {  
        SpringUtil.applicationContext = context;  
    }  
    public static Object getBean(String name){  
        return applicationContext.getBean(name);  
    }  
}  