package com.cn.lcc.util;

import com.cn.lcc.schedue.task.TaskUtil;


/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   线程池关闭
 */
public class DisposeUtil {

    public static void disposeAllTaskPool() {
        TaskUtil.closeAllPool();
    }

}
