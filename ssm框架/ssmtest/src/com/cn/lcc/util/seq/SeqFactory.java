package com.cn.lcc.util.seq;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   序列    用于生成订单号
 */
public class SeqFactory {
	private static int i=100000;
   
   //订单号
   public synchronized static String  GetOrderSeq(String name) {
		  Date now=new Date();
		  SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
		  StringBuilder sb=new StringBuilder(name);
		  sb.append(sdf.format(now));
		  sb.append(i);
		 i++;
		 if(i>999000){i=100000;}
		 return sb.toString();
	}
   
   private static Map<String,String> SeqMapping;
	static{
		SeqMapping=new HashMap<String,String>();
	}
	public static void setSeq(String ours,String others) {
		SeqMapping.put(ours, others);
	}
	public static String getSeq(String ours) {
		
		return SeqMapping.get(ours);
	}
	public static String getSeqRemove(String ours)
	{
		return SeqMapping.remove(ours);
	}
}
