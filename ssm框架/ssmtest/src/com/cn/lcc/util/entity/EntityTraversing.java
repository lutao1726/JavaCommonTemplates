package com.cn.lcc.util.entity;


import java.lang.reflect.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lcc  
 * @date 2017年6月6日
 * @备注   实体遍历类 根据实际需要进行更改
 */
public class EntityTraversing {

	/*
	 * GET 方法
	 */
	public static Map<String, Object> doGet(Object model)
			throws NoSuchMethodException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		Map<String, Object> map = new HashMap<String, Object>();
		Field[] field = model.getClass().getSuperclass().getDeclaredFields();
		// 获取实体类的父类的所有属性，返回Field数组
		String name, method;
		for (int j = 0; j < field.length; j++) { // 遍历所有属性
			name = field[j].getName(); // 获取属性的名字
			method = name.substring(0, 1).toUpperCase() + name.substring(1);
			Method m = model.getClass().getMethod("get" + method);
			Object value = m.invoke(model); // 调用getter方法获取属性值
			if (value != null) {
				map.put(name, value);
			}
		}
		field = model.getClass().getDeclaredFields();
		// 获取实体类的所有属性，返回Field数组
		for (int j = 0; j < field.length; j++) { // 遍历所有属性
			name = field[j].getName(); // 获取属性的名字
			method = name.substring(0, 1).toUpperCase() + name.substring(1);
			Method m = model.getClass().getMethod("get" + method);
			Object value = m.invoke(model); // 调用getter方法获取属性值
			if (value != null) {
				map.put(name, value);
			}
		}
		return map;
	}
	
	/*
	 *  SET 方法
	 */
	public static void doSet(Object model,String[] contents)
			throws Exception {
		Field[] field = model.getClass().getSuperclass().getDeclaredFields();
		String name, method;
		int k=0;
		for (int j = 0; j < field.length; j++,k++) { 
			name = field[j].getName(); 
			method = name.substring(0, 1).toUpperCase() + name.substring(1);
			Method m = model.getClass().getMethod("set"+method,field[j].getType());
			m.invoke(model,contents[k].trim()); 
		}
		field = model.getClass().getDeclaredFields();
		for (int j = 0; j < field.length; j++,k++) { 
			name = field[j].getName(); 
			method = name.substring(0, 1).toUpperCase() + name.substring(1);
			Method m = model.getClass().getMethod("set"+method,field[j].getType());
			m.invoke(model,contents[k].trim()); 
		}
	}
}
