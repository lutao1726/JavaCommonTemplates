package com.cn.lcc.util.request;


/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   清除请求内容
 */
public class ContextManager {

    public static void clearAll() {
        RequestContextUtil.clear();
    }

}
