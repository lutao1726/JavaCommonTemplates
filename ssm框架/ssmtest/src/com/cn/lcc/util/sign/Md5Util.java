package com.cn.lcc.util.sign;

import java.security.MessageDigest;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   md5工具类
 */
public class Md5Util {
	public final static String GetMD5Code(StringBuffer data) {
		 try {
			 MessageDigest md = MessageDigest.getInstance("MD5");
			 md.update(data.toString().getBytes("UTF-8"));
			 byte b[] = md.digest();

			 int i;

			 StringBuffer buf = new StringBuffer("");
			 for (int offset = 0; offset < b.length; offset++) {
			 i = b[offset];
			 if(i<0) i+= 256;
			 if(i<16)
			 buf.append("0");
			 buf.append(Integer.toHexString(i));
			 }
			 return buf.toString().toUpperCase();
			 
			 } catch (Exception e) {
				 return null;
			 }
		 }
	
	public final static String GetMD5(String data) {
		 try {
			 MessageDigest md = MessageDigest.getInstance("MD5");
			 md.update(data.toString().getBytes("UTF-8"));
			 byte b[] = md.digest();

			 int i;

			 StringBuffer buf = new StringBuffer("");
			 for (int offset = 0; offset < b.length; offset++) {
			 i = b[offset];
			 if(i<0) i+= 256;
			 if(i<16)
			 buf.append("0");
			 buf.append(Integer.toHexString(i));
			 }
			 return buf.toString();
			 
			 } catch (Exception e) {
				 e.printStackTrace();
				 return null;
			 }
		 }
	
	
}
