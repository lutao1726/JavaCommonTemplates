package com.cn.lcc.util.tools;

import java.math.BigDecimal;


/**
 * @author lcc  
 * @date 2017年6月13日
 * @备注   换算工具类
 */
public class StringUtil {

	// 元转分
	public static String yuan2fen(String original) {
		try {
			if (original == null || "".equals(original)) {
				return "0";
			}
			BigDecimal bb = BigDecimal.valueOf(Double.parseDouble(original)).multiply(new BigDecimal(100));    
			return String.valueOf(bb.intValue());
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}
	
	
	/**
	 * 分转元
	 * 例子
	 * 1转100
	 * 1.1转110
	 * 1.11 转 111
	 * 1.111转111，舍去后面几位  
	 */
	public static String fen2DoubleYuan(String original) {
		try {
			if (original == null || "".equals(original)) {
				return "0";
			}
			BigDecimal bb = new BigDecimal(Double.parseDouble(original) /100);
			return String.valueOf(bb.doubleValue());
		} catch (Exception e) {
			return "0";
		}
	}
	/**
	 * 分转元
	 * 例子
	 * 1转100
	 * 1.1转110
	 * 1.11 转 111
	 * 1.111转111，舍去后面几位  
	 */
	public static String fen2IntegerYuan(String original) {
		try {
			if (original == null || "".equals(original)) {
				return "0";
			}
			BigDecimal bb = new BigDecimal(Double.parseDouble(original) /100);
			return String.valueOf(bb.intValue());
		} catch (Exception e) {
			return "0";
		}
	}

}
