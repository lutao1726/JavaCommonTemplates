package com.cn.lcc.util.tools;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   内存锁
 */
public class EasyLock {
	private static Map<String,String> locks=null;
	static
	{
		if(locks==null)locks=new HashMap<String, String>();
	}
	
	public synchronized static boolean lock(String lockThis)
	{
		if(locks.get(lockThis)==null||"".equals(locks.get(lockThis)))
		{
			locks.put(lockThis, "1");
			return true;
		}else
		{
			return false;
		}
	}
	
	public synchronized static void unlock(String lockThis)
	{
		locks.remove(lockThis);
	
	}

}
