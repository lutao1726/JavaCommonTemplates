package com.cn.lcc.util.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   时间工具类
 */
public class TimeUtil {
	private static Logger logger = LoggerFactory.getLogger(TimeUtil.class);

	public static SimpleDateFormat df = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static String getNowStr() {
		try {
			return df.format(new Date());
		} catch (Exception e) {
			return "1900-01-01 00:00:00";
		}

	}

	public static String changeDatestr(String date) {
		String newdate = date;
		try {
			Date date1 = df.parse(date);
			newdate = df.format(date1);
		} catch (Exception e) {
			logger.error("日期转化错误:" + e.getMessage());
		}

		return newdate;
	}

}
