package com.cn.lcc.retime;

import java.util.Timer;

/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   定时器    
 * 
 * 此定时器与定时任务区分开来
 * 此定时器仅用于固有参数刷新，例如微信token
 * 
 * spring定时器用于订单查询等与数据库交互
 */
public class timerSet {
	 public Timer timer=null;
	 private static final long PERIOD_DAY = 24 * 60 * 60 * 1000;  
	   public void timerStart(){
		   timer=new Timer();
		   // 所有任务写在task中
//		   timer.schedule(new WeChatKeyRefresh(),1000,7000000);
//		   timer.schedule(new GetCallBackIp(),5000);
	   }
	   public void timerStop(){
	       if(timer!=null) 
	          timer.cancel();
	   }
}
