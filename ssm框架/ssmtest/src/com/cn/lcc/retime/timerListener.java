package com.cn.lcc.retime;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author lcc  
 * @date 2017年6月12日
 * @备注   定时器
 */
public class timerListener  implements ServletContextListener{
	
	private static Logger logger = LoggerFactory.getLogger(timerListener.class);

	private timerSet ts=new timerSet();
	//关闭定时器
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("关闭定时器");
		ts.timerStop();
	}

	//启动定时器
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("启动定时器");
		ts.timerStart();
	}
}
