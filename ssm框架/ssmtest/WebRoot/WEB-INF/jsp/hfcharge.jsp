<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>  
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=0">
<title>话费充值</title>
<link rel="stylesheet" href="../css/weui.min.css">
<link rel="stylesheet" href="../css/example.css">
<link href="../css/jquery-weui.css" rel="stylesheet">
<script type="text/javascript"
	src="../js/jquery-1.7.1.min.js"></script>
<script src="../js/jquery-weui.js"></script>
<script type="text/javascript" src="../js/tools.js"></script>
<script type="text/javascript" src="../js/CZ_SJ.js"></script>
</head>
<body ontouchstart="">
	<form method="post"
		action="http://weixin.170cz.cn/KXJKNEW/hfcharge.html" id="form1">
			<input type="hidden" name="state" id="state"
				value="${state }">
		<div class="container js_container" style="overflow-y:auto;">
			<div class="page slideIn cell">
				<div class="bd">
					<div class="weui_cells" id="chongzhiTop1">
						<div class="weui_cell">
							<div class="weui_cell_hd"></div>
							<div class="weui_cell_bd weui_cell_primary">
								<p>
									<a id="order_link"
										href="http://weixin.170cz.cn/KXJKNEW/order.html"> <img
										src="../images/order.png"
										style="width:18px;padding-bottom:4px;vertical-align:middle;"
										alt=""> <span>订单查询</span>
									</a>

								</p>
							</div>
							<div class="weui_cell_ft">
								<a href="http://weixin.170cz.cn/KXJKNEW/service.html"><img
									src="../images/3.png"
									style="width:18px;padding-bottom:4px;vertical-align:middle;"
									alt=""> <span>在线客服</span></a>
							</div>
						</div>

						<table id="menus" class="quickMenu">
							<tbody>
								<tr>
<!-- 									<td style="background-color:#04BE02;" width="33%"><a -->
<!-- 										id="hfcharge" -->
<!-- 										href="http://weixin.170cz.cn/KXJKNEW/hfcharge.html" -->
<!-- 										style="color:#ffffff;">手机充值</a></td> -->
<!-- 									<td width="33%"><a id="llcharge" -->
<!-- 										href="http://weixin.170cz.cn/KXJKNEW/llcharge.html">流量充值</a></td> -->
<!-- 									<td width="34%"><a id="wlwkcharge" -->
<!-- 										href="http://weixin.170cz.cn/KXJKNEW/wlwkcharge.html">物联网卡</a> -->
<!-- 									</td> -->
<td style="background-color:#04BE02;" width="50%"><a
										id="hfcharge"
										href="http://weixin.170cz.cn/KXJKNEW/hfcharge.html"
										style="color:#ffffff;">手机充值</a></td>
									<td width="50%"><a id="llcharge"
										href="http://weixin.170cz.cn/KXJKNEW/llcharge.html">流量充值</a></td>
									
									<td width="0.1" style="background-color:#c7c7c7;"></td>

								</tr>
							</tbody>
						</table>


					</div>
					<div class="weui_cells">
						<div class="weui_cell weui_cell_select weui_select_before">
							<div class="weui_cell_hd">
								<select class="weui_select" name="select2">
									<option value="1">+86</option>
								</select>
							</div>
							<div class="weui_cell_bd weui_cell_primary">
								<input name="phoneNumber" type="tel" id="phoneNumber"
									class="weui_input" maxlength="13" placeholder="虚商/移动/联通/电信"
									onkeyup="checkPhoneNumber(event);" oninput="queryRegion()">
							</div>


							<div class="weui_cell_ft">
								<i class="weui_icon_info_circle" style="color:#04BE02"></i>
							</div>
						</div>
					</div>

					<div class="weui_cells_title">
						<span id="PhoneInfo"></span>
					</div>

					<div class="weui_grids" id="PriceTag_Div">

						<a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,10);">
							<p class="weui_grid_label">
								10元<br> <span id="priceTag10"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,20);">
							<p class="weui_grid_label">
								20元<br> <span id="priceTag20"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,30);">
							<p class="weui_grid_label">
								30元<br> <span id="priceTag30"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,50);">
							<p class="weui_grid_label">
								50元<br> <span id="priceTag50"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,100);">
							<p class="weui_grid_label">
								100元<br> <span id="priceTag100"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,200);">
							<p class="weui_grid_label">
								200元<br> <span id="priceTag200"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,300);">
							<p class="weui_grid_label">
								300元<br> <span id="priceTag300"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,500);">
							<p class="weui_grid_label">
								500元<br> <span id="priceTag500"></span>
							</p>
						</a> <a href="javascript:;" name="priceTag" class="weui_grid js_grid"
							onclick="setPrice(this,0);">
							<p class="weui_grid_label">
								其他金额<br> <span id="priceTag0"></span>
							</p>
						</a> <input id="chongzhiMoney" type="hidden" value="">
					</div>


					<div class="weui_cells" id="anotherPrice_div" style="display:none">
						<div class="weui_cell">
							<div class="weui_cell_hd">
								<label class="weui_label">充值金额:</label>
							</div>

							<div class="weui_cell_bd weui_cell_primary">
								<input class="weui_input" id="anotherPrice" type="tel"
									placeholder="请输入充值金额" maxlength="6"
									onkeyup="serAnotherPrice();">
							</div>
							<div class="weui_cell_ft">
								<label id="jiage" style="color:red;font-size:14px;"></label>
							</div>
						</div>
					</div>
					<div style="padding-bottom:13px;"></div>
					<div class="bd spacing" style="padding-top:13px;">
						<a href="javascript:;" id="submitProm"
							class="weui_btn weui_btn_primary" onclick="submitForm();">提交充值</a>
					</div>
					<div id="wxtips">
						<div style="color:#888888;font-size:11px;padding:0px 11px;"
							id="tips">【温馨提示】：</div>
						<div id="commonTips" style="padding:0px 15px;">
							<p style="color:#888888;font-size:11px;line-height:17px;">
								1.请您仔细核对充值号码，充错号码将无法退款；<br /> 2.支付成功后预计10分钟之内到账，充值高峰期可能会有延迟；<br />
								3.若充值失败，支付金额将原路返回；<br /> 4.详细充值记录可拨打运营商客服电话咨询。
							</p>
						</div>
						<div id="telecomTips" style="padding:0px 15px;display:none;">
							<p style="color:#888888;font-size:11px;line-height:17px;">
								1.此充值为卡充值，中国电信不提供话费充值发票，如有发票问题，详询客服热线：4000817007；<br />
								2.请您仔细核对充值号码，充错号码将无法退款；<br /> 3.支付成功后预计10分钟之内到账，充值高峰期可能会有延迟；<br />
								4.若充值失败，支付金额将原路返回；<br /> 5.详细充值记录可拨打运营商客服电话咨询。
							</p>
						</div>
					</div>

				</div>
			</div>



			<div id="loadingToast" class="weui_loading_toast"
				style="display:none;">
				<div class="weui_mask_transparent"></div>
				<div class="weui_toast">
					<div class="weui_loading">
						<!-- :) -->
						<div class="weui_loading_leaf weui_loading_leaf_0"></div>
						<div class="weui_loading_leaf weui_loading_leaf_1"></div>
						<div class="weui_loading_leaf weui_loading_leaf_2"></div>
						<div class="weui_loading_leaf weui_loading_leaf_3"></div>
						<div class="weui_loading_leaf weui_loading_leaf_4"></div>
						<div class="weui_loading_leaf weui_loading_leaf_5"></div>
						<div class="weui_loading_leaf weui_loading_leaf_6"></div>
						<div class="weui_loading_leaf weui_loading_leaf_7"></div>
						<div class="weui_loading_leaf weui_loading_leaf_8"></div>
						<div class="weui_loading_leaf weui_loading_leaf_9"></div>
						<div class="weui_loading_leaf weui_loading_leaf_10"></div>
						<div class="weui_loading_leaf weui_loading_leaf_11"></div>
					</div>
					<p class="weui_toast_content">数据加载中</p>
				</div>
			</div>
		</div>




	</form>

	<div id="qb-sougou-search" style="display: none; opacity: 0;">
		<p>搜索</p>
		<p class="last-btn">复制</p>
		<iframe src=""></iframe>
	</div>
</body>
</html>