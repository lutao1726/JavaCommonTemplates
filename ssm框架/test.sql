/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.38 : Database - ssm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ssm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ssm`;

/*Table structure for table `test1` */

DROP TABLE IF EXISTS `test1`;

CREATE TABLE `test1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderNo` varchar(30) NOT NULL,
  `accountNo` varchar(13) NOT NULL,
  `orderStatus` varchar(50) NOT NULL DEFAULT '等待付款' COMMENT '订单状态',
  `payStatus` varchar(50) NOT NULL DEFAULT '未付款' COMMENT '支付状态',
  `description` varchar(50) DEFAULT NULL COMMENT '备注',
  `amount` varchar(10) DEFAULT NULL COMMENT '商品原价',
  `payamount` varchar(10) DEFAULT NULL COMMENT '微信支付金额',
  `createDate` datetime NOT NULL,
  `createBy` varchar(10) DEFAULT NULL,
  `updateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updateBy` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `test1` */

insert  into `test1`(`id`,`orderNo`,`accountNo`,`orderStatus`,`payStatus`,`description`,`amount`,`payamount`,`createDate`,`createBy`,`updateDate`,`updateBy`) values (3,'123123','1234567','等待付款','未付款',NULL,NULL,NULL,'2017-06-13 22:41:13',NULL,'2017-06-13 22:41:13',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
